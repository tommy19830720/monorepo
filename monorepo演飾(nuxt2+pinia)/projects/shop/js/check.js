class Sohp {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  swim() {
    return "this is shop export:" + "name=" + this.name;
  }

  print() {
    return "🐟 ";
  }
}

module.exports = Sohp;
