const Fish = require("share/js/Fish");
const fish = new Fish();
document.getElementById("app").textContent = fish.print();
fish.swim();

class Sohp {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  swim() {
    console.log("this is shop export:" + "name=" + this.name);
  }

  print() {
    return "🐟 ";
  }
}

module.exports = Sohp;

/*const shop = new Sohp();
shop.name = "tommy";
shop.age = 39;
console.log(shop);
shop.swim();*/
