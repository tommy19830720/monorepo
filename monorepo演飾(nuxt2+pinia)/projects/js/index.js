exports.RootMenuMap = [
  { key: "systemManagement123", path: "system", sort: 7, icon: "mdi-cog" },
  {
    key: "memberManagement",
    path: "member",
    sort: 1,
    icon: "mdi-account-circle",
  },
  {
    key: "eventManagement",
    path: "event",
    sort: 8,
    icon: "mdi-controller-classic",
  },
  { key: "dashboard", path: "dashboard", sort: 0, icon: "mdi-view-dashboard" },
  { key: "accountManagement", path: "report", sort: 3, icon: "mdi-file-chart" },
  {
    key: "noticeManagement",
    path: "notify",
    sort: 2,
    icon: "mdi-message-text",
  },
  {
    key: "financeManagement",
    path: "finance",
    sort: 4,
    icon: "mdi-clipboard-flow",
  },
  {
    key: "activityManagement",
    path: "activity",
    sort: 5,
    icon: "mdi-calendar-text",
  },
  { key: "branchManagement", path: "branch", sort: 9, icon: "mdi-sitemap" },
  {
    key: "tgBusinessManagement",
    path: "wechat",
    sort: 10,
    icon: "mdi-sitemap",
  },
];
