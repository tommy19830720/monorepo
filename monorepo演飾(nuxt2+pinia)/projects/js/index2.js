export const MenuMap = [
  { key: "systemManagement456", path: "system", sort: 7, icon: "mdi-cog" },
  {
    key: "memberManagement",
    path: "member",
    sort: 1,
    icon: "mdi-account-circle",
  },
  {
    key: "eventManagement",
    path: "event",
    sort: 8,
    icon: "mdi-controller-classic",
  },
  { key: "dashboard", path: "dashboard", sort: 0, icon: "mdi-view-dashboard" },
  /* { key: 'betManagement', path: 'bet', sort: 8, icon: 'mdi-note-text' }, */
  { key: "accountManagement", path: "report", sort: 3, icon: "mdi-file-chart" },
  {
    key: "noticeManagement",
    path: "notify",
    sort: 2,
    icon: "mdi-message-text",
  },
  {
    key: "financeManagement",
    path: "finance",
    sort: 4,
    icon: "mdi-clipboard-flow",
  },
  {
    key: "activityManagement",
    path: "activity",
    sort: 5,
    icon: "mdi-calendar-text",
  },
  // { key: 'micronetManagement', path: 'micronet', sort: 6, icon: 'mdi-file-tree' },
  // tgBusinessManagement // tgBusinessReward
  { key: "branchManagement", path: "branch", sort: 9, icon: "mdi-sitemap" },
  {
    key: "tgBusinessManagement",
    path: "wechat",
    sort: 10,
    icon: "mdi-sitemap",
  },
  // { key: 'dashboard', path: '', sort: 0, icon: 'mdi-sitemap' },
];
