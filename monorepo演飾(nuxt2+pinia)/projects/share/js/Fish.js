class Fish {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  swim() {
    console.log("this is Fish export");
  }

  print() {
    return "🐟 ";
  }
}

module.exports = Fish;
