import { defineStore } from "pinia";
export const useStore = defineStore({
  id: "game",
  // arrow function recommended for full type inference
  state: () => {
    return {
      // all these properties will have their type inferred automatically
      counter: 10,
      name: "this is zoo/store export",
      isAdmin: true,
    };
  },
  actions: {
    hit() {
      this.counter++;
    },
  },

  getters: {
    getCount: (state) => state.counter,
    getUser: (state) => {
      state.name;
    },
  },
});
